#ifndef PULSEDIAL_H_
#define PULSEDIAL_H_

#include "Pin.h"


#define DEBOUNCE_THRESHOLD 				5


class PulseDial {

private:

public:
	Pin		pulse_pin;
	Pin		dial_pin;
	Pin		handset_pin;


	PulseDial(void);
	bool is_handset_picked_up(void);
	bool is_dialing(void);
	bool is_number_valid(uint32_t);
	void dial(uint32_t, uint32_t *, uint8_t);
};



#endif /* PULSEDIAL_H_ */
