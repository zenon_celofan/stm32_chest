#include "Telephone.h"
#include "Mp3Player.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "delay.h"

Telephone::Telephone(void) {

	pulse_gpio = GPIOB;
	pulse_pin = GPIO_Pin_1;

	dial_gpio = GPIOB;
	dial_pin = GPIO_Pin_0;

	handset_gpio = GPIOB;
	handset_pin = GPIO_Pin_9;


	GPIO_InitTypeDef  GPIO_InitStructure;

	//pulse
	if (pulse_gpio == GPIOA) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	}
	else if (pulse_gpio == GPIOB) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	}
	else if (pulse_gpio == GPIOC) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	}

	GPIO_InitStructure.GPIO_Pin = pulse_pin;   // DIAL | PULSE
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(pulse_gpio, &GPIO_InitStructure);

	//dial
	if (dial_gpio == GPIOA) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	}
	else if (dial_gpio == GPIOB) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	}
	else if (dial_gpio == GPIOC) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	}

	GPIO_InitStructure.GPIO_Pin = dial_pin;   // DIAL | PULSE
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(dial_gpio, &GPIO_InitStructure);

	//handset pin
	if (handset_gpio == GPIOA) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	}
	else if (handset_gpio == GPIOB) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	}
	else if (handset_gpio == GPIOC) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	}

	GPIO_InitStructure.GPIO_Pin = handset_pin;   // HANDSET
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(handset_gpio, &GPIO_InitStructure);

} //Telephone


bool Telephone::is_handset_picked_up(void) {

	static	uint16_t	debounce;
	bool	handset_picked_up;

	if (GPIO_ReadInputDataBit(handset_gpio, handset_pin) == Bit_SET) {
		if (debounce > DEBOUNCE_THRESHOLD) {
			handset_picked_up = true;
		} else {
			handset_picked_up = false;
			debounce++;
		}
	} else {
		handset_picked_up = false;
		debounce = 0;
	}

	return handset_picked_up;

} //is_handset_picked_up()


bool Telephone::is_dialing(void) {

	int8_t	dial_debounce = 0;

	while (1) {
		if (GPIO_ReadInputDataBit(dial_gpio, dial_pin) == Bit_SET) {
			dial_debounce++;
		} else {
			dial_debounce--;
		}

		if (dial_debounce < 0 - DEBOUNCE_THRESHOLD) {
			return true;
		}

		if (dial_debounce > DEBOUNCE_THRESHOLD) {
			return false;
		}

		delay(5);
	}
} //is_dialing()

bool Telephone::is_number_valid(uint32_t n) {

	if ((n > 99999999) && (n < 1000000000)) {   //number is exactly 9-digit
		return true;
	}

	return false;

} //is_number_valid()


void Telephone::dial(uint32_t number, uint32_t * phone_book, uint8_t phone_book_size) {

	uint8_t to_be_played;

	for (uint8_t i = 0; i < phone_book_size; i++) {
		if (phone_book[i] == number) {
			to_be_played = FIRST_NUMBER_SOUND + i;
			break;
		}
	}

	//mp3_player.play(to_be_played);

} //dial()
