/**	
 * |----------------------------------------------------------------------
 * | Edited by PhanDAT 23/09/2015
 * | Copyright (C) Tilen Majerle, 2014
 * | 
 * | This program is free software: you can redistribute it and/or modify
 * | it under the terms of the GNU General Public License as published by
 * | the Free Software Foundation, either version 3 of the License, or
 * | any later version.
 * |  
 * | This program is distributed in the hope that it will be useful,
 * | but WITHOUT ANY WARRANTY; without even the implied warranty of
 * | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * | GNU General Public License for more details.
 * | 
 * | You should have received a copy of the GNU General Public License
 * | along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * |----------------------------------------------------------------------
 */
#include "RC522.h"
#include <stdio.h>
//#include "systick.h"
#include <STM32F10X_SPI.h>
#include <STM32F10X_GPIO.h>
#include <STM32F10X_RCC.h> 

#include "micros.h"

extern RC522 rfid_reader;

RC522::RC522(SPI_TypeDef* spi_x_param, uint8_t cs_pin_param, uint8_t reset_pin_param, uint8_t irq_pin_param) :
			rc522_cs_pin(cs_pin_param, Bit_SET),
			rc522_reset_pin(reset_pin_param),
			rc522_irq_pin(irq_pin_param) {

	SPI_InitTypeDef  SPI_InitStructure;
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8; // 72Mhz / 8 = 9Mhz
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;

	rc522_spi = Spi(spi_x_param, &SPI_InitStructure),

	micros_init();

	reset();

	write_register(MFRC522_REG_T_MODE, 0x80);	//0x8D
	write_register(MFRC522_REG_T_PRESCALER, 0xA9);	//0x3E
	write_register(MFRC522_REG_T_RELOAD_L, 0xE8);  	//30
	write_register(MFRC522_REG_T_RELOAD_H, 0x03);		//0

	//TM_MFRC522_WriteRegister(MFRC522_REG_RF_CFG, 0x70);	/* 48dB gain */
	write_register(MFRC522_REG_TX_AUTO, 0x40);
	write_register(MFRC522_REG_MODE, 0x3D);

	antenna_enable();

} //RC522()



void RC522::write_register(uint8_t addr, uint8_t val) {
	rc522_cs_pin.digital_write(Bit_RESET);
	rc522_spi.write_byte((addr << 1) & 0x7E);
	rc522_spi.write_byte(val);
	rc522_cs_pin.digital_write(Bit_SET);
} //write_register()



uint8_t RC522::read_register(uint8_t addr) {
	uint8_t val;

	rc522_cs_pin.digital_write(Bit_RESET);
	rc522_spi.write_byte(((addr << 1) & 0x7E) | 0x80);
	val = rc522_spi.read_byte();
	rc522_cs_pin.digital_write(Bit_SET);

	return val;
} //read_register()



void RC522::reset(void) {
	rc522_cs_pin.digital_write(Bit_RESET);
	write_register(MFRC522_REG_COMMAND, PCD_RESETPHASE);
	rc522_cs_pin.digital_write(Bit_SET);
} //reset()



void RC522::antenna_enable(void) {
	uint8_t temp;

	rc522_cs_pin.digital_write(Bit_RESET);
	temp = read_register(MFRC522_REG_TX_CONTROL);
	rc522_cs_pin.digital_write(Bit_SET);

	if (!(temp & 0x03)) {
		rc522_cs_pin.digital_write(Bit_RESET);
		rfid_reader.write_register(MFRC522_REG_TX_CONTROL, temp | 0x03);
		rc522_cs_pin.digital_write(Bit_SET);
	}
} //antenna_enable()



uint8_t RC522::get_firmware_version(void) {
	return read_register(MFRC522_REG_VERSION);
} //get_firmware_version()







 //-------------------------------------------------
//void TM_MFRC522_WriteRegister(uint8_t addr, uint8_t val) {
//	rfid_reader.write_register(addr, val);
//}
//-------------------------------------------------
//uint8_t TM_MFRC522_ReadRegister(uint8_t addr) {
//	return rfid_reader.read_register(addr);
//}
//-------------------------------------------------

//-------------------------------------------------
//void TM_MFRC522_AntennaOff(void) {
//	rfid_reader.write_register(MFRC522_REG_TX_CONTROL, rfid_reader.read_register(MFRC522_REG_TX_CONTROL) & (~0x03));//(MFRC522_REG_TX_CONTROL, 0x03);
//}
//-------------------------------------------------
void TM_MFRC522_Reset(void) {
	rfid_reader.write_register(MFRC522_REG_COMMAND, PCD_RESETPHASE);
}
//-------------------------------------------------
//uint8_t getFirmwareVersion()
//{
//  uint8_t response;
//  response = rfid_reader.read_register(MFRC522_REG_VERSION);
//  return response;
//}
//-------------------------------------------------
TM_MFRC522_Status_t TM_MFRC522_Request(uint8_t reqMode, uint8_t* TagType) {
	TM_MFRC522_Status_t status;  
	uint16_t backBits;			//The received data bits

	rfid_reader.write_register(MFRC522_REG_BIT_FRAMING, 0x07);		//TxLastBists = BitFramingReg[2..0]	???

	TagType[0] = reqMode;
	status = TM_MFRC522_ToCard(PCD_TRANSCEIVE, TagType, 1, TagType, &backBits);

	if ((status != MI_OK) || (backBits != 0x10)) {    
		status = MI_ERR;
	}

	return status;
}

//-------------------------------------------------
TM_MFRC522_Status_t TM_MFRC522_Check(uint8_t* id) {
	TM_MFRC522_Status_t status;
	//Find cards, return card type
	status = TM_MFRC522_Request(PICC_REQIDL, id);	
	if (status == MI_OK) {
		//Card detected
		//Anti-collision, return card serial number 4 bytes
		status = TM_MFRC522_Anticoll(id);	
	}
	TM_MFRC522_Halt();			//Command card into hibernation 

	return status;
}
//-------------------------------------------------
TM_MFRC522_Status_t TM_MFRC522_Compare(uint8_t* CardID, uint8_t* CompareID) {
	uint8_t i;
	for (i = 0; i < 5; i++) {
		if (CardID[i] != CompareID[i]) {
			return MI_ERR;
		}
	}
	return MI_OK;
} 
//-------------------------------------------------
TM_MFRC522_Status_t TM_MFRC522_ToCard(uint8_t command, uint8_t* sendData, uint8_t sendLen, uint8_t* backData, uint16_t* backLen) {
	TM_MFRC522_Status_t status = MI_ERR;
	uint8_t irqEn = 0x00;
	uint8_t waitIRq = 0x00;
	uint8_t lastBits;
	uint8_t n;
	uint16_t i;

	switch (command) {
		case PCD_AUTHENT: {
			irqEn = 0x12;
			waitIRq = 0x10;
			break;
		}
		case PCD_TRANSCEIVE: {
			irqEn = 0x77;
			waitIRq = 0x30;
			break;
		}
		default:
			break;
	}

	rfid_reader.write_register(MFRC522_REG_COMM_IE_N, irqEn | 0x80);
	rfid_reader.write_register(MFRC522_REG_COMM_IRQ, rfid_reader.read_register(MFRC522_REG_COMM_IRQ) & (~0x80));//(MFRC522_REG_COMM_IRQ, 0x80);
	rfid_reader.write_register(MFRC522_REG_FIFO_LEVEL, rfid_reader.read_register(MFRC522_REG_FIFO_LEVEL) | 0x80);//(MFRC522_REG_FIFO_LEVEL, 0x80);

	rfid_reader.write_register(MFRC522_REG_COMMAND, PCD_IDLE);

	//Writing data to the FIFO
	for (i = 0; i < sendLen; i++) {   
		rfid_reader.write_register(MFRC522_REG_FIFO_DATA, sendData[i]);
	}

	//Execute the command
	rfid_reader.write_register(MFRC522_REG_COMMAND, command);
	if (command == PCD_TRANSCEIVE) {    
		rfid_reader.write_register(MFRC522_REG_BIT_FRAMING, rfid_reader.read_register(MFRC522_REG_BIT_FRAMING) | 0x80);//(MFRC522_REG_BIT_FRAMING, 0x80);		//StartSend=1,transmission of data starts
	}   

	//Waiting to receive data to complete
	i = 2000;	//i according to the clock frequency adjustment, the operator M1 card maximum waiting time 25ms???
	do {
		//CommIrqReg[7..0]
		//Set1 TxIRq RxIRq IdleIRq HiAlerIRq LoAlertIRq ErrIRq TimerIRq
		n = rfid_reader.read_register(MFRC522_REG_COMM_IRQ);
		i--;
	} while ((i!=0) && !(n&0x01) && !(n&waitIRq));

	rfid_reader.write_register(MFRC522_REG_BIT_FRAMING, rfid_reader.read_register(MFRC522_REG_BIT_FRAMING) & (~0x80));//(MFRC522_REG_BIT_FRAMING, 0x80);			//StartSend=0

	if (i != 0)  {
		if (!(rfid_reader.read_register(MFRC522_REG_ERROR) & 0x1B)) {
			status = MI_OK;
			if (n & irqEn & 0x01) {   
				status = MI_NOTAGERR;			
			}

			if (command == PCD_TRANSCEIVE) {
				n = rfid_reader.read_register(MFRC522_REG_FIFO_LEVEL);
				lastBits = rfid_reader.read_register(MFRC522_REG_CONTROL) & 0x07;
				if (lastBits) {   
					*backLen = (n - 1) * 8 + lastBits;   
				} else {   
					*backLen = n * 8;   
				}

				if (n == 0) {   
					n = 1;    
				}
				if (n > MFRC522_MAX_LEN) {   
					n = MFRC522_MAX_LEN;   
				}

				//Reading the received data in FIFO
				for (i = 0; i < n; i++) {   
					backData[i] = rfid_reader.read_register(MFRC522_REG_FIFO_DATA);
				}
			}
		} else {   
			status = MI_ERR;  
		}
	}

	return status;
}
//-------------------------------------------------
TM_MFRC522_Status_t TM_MFRC522_Anticoll(uint8_t* serNum) {
	TM_MFRC522_Status_t status;
	uint8_t i;
	uint8_t serNumCheck = 0;
	uint16_t unLen;

	rfid_reader.write_register(MFRC522_REG_BIT_FRAMING, 0x00);		//TxLastBists = BitFramingReg[2..0]

	serNum[0] = PICC_ANTICOLL;
	serNum[1] = 0x20;
	status = TM_MFRC522_ToCard(PCD_TRANSCEIVE, serNum, 2, serNum, &unLen);

	if (status == MI_OK) {
		//Check card serial number
		for (i = 0; i < 4; i++) {   
			serNumCheck ^= serNum[i];
		}
		if (serNumCheck != serNum[i]) {   
			status = MI_ERR;    
		}
	}
	return status;
} 

void TM_MFRC522_CalculateCRC(uint8_t*  pIndata, uint8_t len, uint8_t* pOutData) {
	uint8_t i, n;

	rfid_reader.write_register(MFRC522_REG_DIV_IRQ, rfid_reader.read_register(MFRC522_REG_DIV_IRQ) & (~0x04));//(MFRC522_REG_DIV_IRQ, 0x04);			//CRCIrq = 0
	rfid_reader.write_register(MFRC522_REG_FIFO_LEVEL, rfid_reader.read_register(MFRC522_REG_FIFO_LEVEL) | 0x80);//(MFRC522_REG_FIFO_LEVEL, 0x80);			//Clear the FIFO pointer
	//Write_MFRC522(CommandReg, PCD_IDLE);

	//Writing data to the FIFO	
	for (i = 0; i < len; i++) {   
		rfid_reader.write_register(MFRC522_REG_FIFO_DATA, *(pIndata+i));
	}
	rfid_reader.write_register(MFRC522_REG_COMMAND, PCD_CALCCRC);

	//Wait CRC calculation is complete
	i = 0xFF;
	do {
		n = rfid_reader.read_register(MFRC522_REG_DIV_IRQ);
		i--;
	} while ((i!=0) && !(n&0x04));			//CRCIrq = 1

	//Read CRC calculation result
	pOutData[0] = rfid_reader.read_register(MFRC522_REG_CRC_RESULT_L);
	pOutData[1] = rfid_reader.read_register(MFRC522_REG_CRC_RESULT_M);
}
//-------------------------------------------------
uint8_t TM_MFRC522_SelectTag(uint8_t* serNum) {
	uint8_t i;
	TM_MFRC522_Status_t status;
	uint8_t size;
	uint16_t recvBits;
	uint8_t buffer[9]; 

	buffer[0] = PICC_SElECTTAG;
	buffer[1] = 0x70;
	for (i = 0; i < 5; i++) {
		buffer[i+2] = *(serNum+i);
	}
	TM_MFRC522_CalculateCRC(buffer, 7, &buffer[7]);		//??
	status = TM_MFRC522_ToCard(PCD_TRANSCEIVE, buffer, 9, buffer, &recvBits);

	if ((status == MI_OK) && (recvBits == 0x18)) {   
		size = buffer[0]; 
	} else {   
		size = 0;    
	}

	return size;
}
//-------------------------------------------------
TM_MFRC522_Status_t TM_MFRC522_Auth(uint8_t authMode, uint8_t BlockAddr, uint8_t* Sectorkey, uint8_t* serNum) {
	TM_MFRC522_Status_t status;
	uint16_t recvBits;
	uint8_t i;
	uint8_t buff[12]; 

	//Verify the command block address + sector + password + card serial number
	buff[0] = authMode;
	buff[1] = BlockAddr;
	for (i = 0; i < 6; i++) {    
		buff[i+2] = *(Sectorkey+i);   
	}
	for (i=0; i<4; i++) {    
		buff[i+8] = *(serNum+i);   
	}
	status = TM_MFRC522_ToCard(PCD_AUTHENT, buff, 12, buff, &recvBits);

	if ((status != MI_OK) || (!(rfid_reader.read_register(MFRC522_REG_STATUS2) & 0x08))) {
		status = MI_ERR;   
	}

	return status;
}
//-------------------------------------------------
TM_MFRC522_Status_t TM_MFRC522_Read(uint8_t blockAddr, uint8_t* recvData) {
	TM_MFRC522_Status_t status;
	uint16_t unLen;

	recvData[0] = PICC_READ;
	recvData[1] = blockAddr;
	TM_MFRC522_CalculateCRC(recvData,2, &recvData[2]);
	status = TM_MFRC522_ToCard(PCD_TRANSCEIVE, recvData, 4, recvData, &unLen);

	if ((status != MI_OK) || (unLen != 0x90)) {
		status = MI_ERR;
	}

	return status;
}
//-------------------------------------------------
TM_MFRC522_Status_t TM_MFRC522_Write(uint8_t blockAddr, uint8_t* writeData) {
	TM_MFRC522_Status_t status;
	uint16_t recvBits;
	uint8_t i;
	uint8_t buff[18]; 

	buff[0] = PICC_WRITE;
	buff[1] = blockAddr;
	TM_MFRC522_CalculateCRC(buff, 2, &buff[2]);
	status = TM_MFRC522_ToCard(PCD_TRANSCEIVE, buff, 4, buff, &recvBits);

	if ((status != MI_OK) || (recvBits != 4) || ((buff[0] & 0x0F) != 0x0A)) {   
		status = MI_ERR;   
	}

	if (status == MI_OK) {
		//Data to the FIFO write 16Byte
		for (i = 0; i < 16; i++) {    
			buff[i] = *(writeData+i);   
		}
		TM_MFRC522_CalculateCRC(buff, 16, &buff[16]);
		status = TM_MFRC522_ToCard(PCD_TRANSCEIVE, buff, 18, buff, &recvBits);

		if ((status != MI_OK) || (recvBits != 4) || ((buff[0] & 0x0F) != 0x0A)) {   
			status = MI_ERR;   
		}
	}

	return status;
}

void TM_MFRC522_Halt(void) {
	uint16_t unLen;
	uint8_t buff[4]; 

	buff[0] = PICC_HALT;
	buff[1] = 0;
	TM_MFRC522_CalculateCRC(buff, 2, &buff[2]);

	TM_MFRC522_ToCard(PCD_TRANSCEIVE, buff, 4, buff, &unLen);
}

