#ifndef WEBSERVER_H_
#define WEBSERVER_H_

#include "stm32f10x.h"


#define EXIT_SUCCESS		0
#define EXIT_FAILURE 		1



class WebServer {

private:




public:

	WebServer(void);

};


void 	ethernet_init(void);
uint8_t web_socket_init(uint8_t, uint16_t);
uint8_t web_server_check_for_job(uint8_t);
void	print_webpage(void);
void	ethernet_turn_on_and_init(void);
void	ethernet_turn_off(void);


#endif /* WEBSERVER_H_ */
