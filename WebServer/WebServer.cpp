#include "WebServer.h"
#include "stm32f10x.h"
#include "spi1.h"
#include "stm32f10x_gpio.h"
#include "wizchip_conf.h"
#include "socket.h"
#include "serial3.h"
#include "delay.h"
#include "pc13led.h"

#include <stdio.h>
#include <string.h>

int16_t retVal, sockStatus;
int16_t rcvLen;
uint8_t rcvBuf[1000];
uint8_t bufSize[] = {2, 2, 2, 2, 2, 2, 2, 2};
uint8_t led_status;

wiz_NetInfo netInfo = { .mac 	= {0xde, 0xad, 0xbe, 0xef, 0xca, 0xfe},	// Mac address
                        .ip 	= {192, 168, 10, 20},					// IP address
                        .sn 	= {255, 255, 255, 0},					// Subnet mask
                        .gw 	= {192, 168, 10, 10},						// Gateway address
						.dns	= {8, 8, 8, 8},
						.dhcp   = (dhcp_mode) 1
};

wiz_NetInfo netInfo_check;


WebServer::WebServer(void) {

	reg_wizchip_cs_cbfunc(cs_sel, cs_desel);
	reg_wizchip_spi_cbfunc(spi_rb, spi_wb);

	spi1_init();
}


void ethernet_init(void) {
	serial3_send("Ethernet init begin\n");

	reg_wizchip_cs_cbfunc(cs_sel, cs_desel);
	reg_wizchip_spi_cbfunc(spi_rb, spi_wb);


	while(1) {
		char msg[20];
		wizchip_init(bufSize, bufSize);
		wizchip_setnetinfo(&netInfo);
		wizchip_getnetinfo(&netInfo_check);

		sprintf(msg, "ip: %u.%u.%u.%u", netInfo_check.ip[0], netInfo_check.ip[1], netInfo_check.ip[2], netInfo_check.ip[3]);
		serial3_send(msg);
		if ((netInfo.ip[0] == netInfo_check.ip[0]) &&
			(netInfo.ip[1] == netInfo_check.ip[1]) &&
			(netInfo.ip[2] == netInfo_check.ip[2]) &&
			(netInfo.ip[3] == netInfo_check.ip[3])) {
				serial3_send(" - OK\n");
				break;
		}
		else {
			serial3_send(" - not OK, retrying...\n");
			//ethernet_turn_off();
			delay(1000);
			//ethernet_turn_on_and_init();
		}
	}
	serial3_send("Ethernet init end - OK.\n\n");
} //ethernet_init()


uint8_t web_socket_init(uint8_t socket_number, uint16_t port) {
	char msg[100];
	//open socket
	sprintf(msg, "Opening socket [%u] on port [%u]: ", socket_number, port);
	serial3_send(msg);
	retVal = socket(socket_number, Sn_MR_TCP, port, 0);
	sprintf(msg, "(%i)", retVal);
	serial3_send(msg);
	if (retVal == socket_number) {
		serial3_send(" - OK.\n");
	} else {
		serial3_send(" - not OK.\n");
		return EXIT_FAILURE;
	}
	delay(100);

	//check socket status
	serial3_send("Socket status: ");
	retVal = getSn_SR(socket_number);
	sprintf(msg, "(0x%x)", retVal);
	serial3_send(msg);
	if (retVal == 0x13) { //SOCK_INIT
		serial3_send(" - (SOCK_INIT): OK.\n");
	} else {
		serial3_send(" - not OK.\n");
		return EXIT_FAILURE;
	}
	delay(100);

	//enable listen mode for web socket (2)
	if (socket_number == 2) {
		retVal = listen(2);
		serial3_send("Socket listen mode: ");
		retVal = getSn_SR(2);
		sprintf(msg, "(0x%x)", retVal);
		serial3_send(msg);
		if (retVal == 0x14) { //SOCKET_LISTEN
			serial3_send(" - OK.\n");
		} else {
			serial3_send(" - not OK.\n");
			return EXIT_FAILURE;
		}
		delay(100);
	}

	return retVal;
} //web_socket_init()


uint8_t web_server_check_for_job(uint8_t flag) {
	char msg[50];
	uint8_t remoteIP[4];
	uint16_t remotePort;
	int8_t retVal;

	uint8_t targetIP[4] = {192, 168, 10, 10};   //send action signal to this address

	//web socket [2] check
	if ((sockStatus = getSn_SR(2)) == SOCK_ESTABLISHED) {
		serial3_send("Connection established.\n");
		// Retrieving remote peer IP and port number
		getsockopt(2, SO_DESTIP, remoteIP);
		getsockopt(2, SO_DESTPORT, (uint8_t*)&remotePort);
		sprintf(msg, "request from: %u.%u.%u.%u:%u\n", remoteIP[0], remoteIP[1], remoteIP[2], remoteIP[3], remotePort);
		serial3_send(msg);
		//receive request
		serial3_send("Receiving request:\n");
		retVal = recv(2, rcvBuf, 20);
		if (retVal < 0) {
			sprintf(msg, "error: 0x%x\n", retVal);
			serial3_send(msg);
			//return EXIT_FAILURE;
		} else if (retVal == 0) {
			serial3_send("no data received.\n");
			return EXIT_FAILURE;
		} else {
			for (uint8_t i = 0; i < retVal; i++) {
				serial3_send_char(rcvBuf[i]);
			}
			serial3_send("\n");

			//respond with web-page
			if (strncmp("GET ", (char *) &(rcvBuf[0]), 4) == 0) {
				print_webpage();
				serial3_send("webpage sent.\n");
			} else {
				retVal = recv(2, rcvBuf, 100);
				sprintf(msg, "retVal: %i\n", retVal);
				serial3_send(msg);
				retVal = send(2, (uint8_t *) "ok", strlen("ok"));
			}
		} //if (retVal == 0)

		//delay(50);
		disconnect(2);
		close(2);
		serial3_send("web socket [2] closed.\n");
		delay(200);
		//web_socket_init(2, 80);
	} //if ((sockStatus = getSn_SR(2)) == SOCK_ESTABLISHED)

	// web socket [2] re-open
	if ((sockStatus = getSn_SR(2)) != SOCK_LISTEN) {
			ethernet_turn_off();
			delay(100);
			ethernet_turn_on_and_init();
			web_socket_init(2, 80);
	} //if ((sockStatus = getSn_SR(2)) != SOCK_LISTEN)


	//send TCP "action" to LCD screen
	if (flag != 0) {
		serial3_send("Trying to send action signal.\n");

		while (web_socket_init(1, 1234) != SOCK_INIT) {
			ethernet_turn_off();
			delay(100);
			ethernet_turn_on_and_init();
			serial3_send("Cannot create socket, retrying.\n");
		}
		retVal = connect(1, targetIP, 80);			//random port 1234;

		if (retVal == 1) {
			serial3_send("connected.\n");
			send(1, (uint8_t *) "GET /action", 11);
			serial3_send("ACTION signal sent.\n");
		} else {
			sprintf(msg, "NOT connected! (error: 0x%i)\n", retVal);
			serial3_send(msg);
		}

		serial3_send("Closing connection.\n");
		disconnect(1);
		close(1);
	}



	return EXIT_SUCCESS;
} //web_server_check_for_job()


void print_webpage(void) {
	uint8_t webpage[1000];

	//EE_ReadVariable(VirtAddVarTab[0], &val);
	sprintf((char *) webpage, "<pre>Noise Sensor\nIP: %u.%u.%u.%u\n\nWhen noise meter is red-full it sends TCP request: 192.168.10.10/action\n</pre>", netInfo.ip[0], netInfo.ip[1], netInfo.ip[2], netInfo.ip[3]);
	retVal = send(2, webpage, strlen((const char *) webpage));			//) == (int16_t)strlen(webpage);
} //print_webpage()


void ethernet_turn_on_and_init(void) {
	GPIO_WriteBit(GPIOA, GPIO_Pin_3, Bit_SET);
	delay(1000);
	ethernet_init();
} //ethernet_turn_on()


void ethernet_turn_off(void) {
	serial3_send("resetting ethernet module\n");

	GPIO_WriteBit(GPIOA, GPIO_Pin_3, Bit_RESET);
	delay(10);
} //ethernet_turn_off()
