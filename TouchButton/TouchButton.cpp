#include "TouchButton.h"

TouchButton::TouchButton(uint16_t _pin, BitAction _active_state, FunctionalState _assign_interrupt) : active_state(_active_state) {
	if (active_state == Bit_SET) {
		button = Pin(_pin, GPIO_Mode_IPD, _assign_interrupt);
	} else {
		button = Pin(_pin, GPIO_Mode_IPU, _assign_interrupt);
	}
} //TouchButton()


bool TouchButton::is_button_pressed() {
	return button.digital_read(); // == (uint8_t) active_state ? true : false;
} //is_button_pressed()
