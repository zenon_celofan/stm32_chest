#ifndef TOUCHBUTTON_TOUCHBUTTON_H_
#define TOUCHBUTTON_TOUCHBUTTON_H_

#include "bluepill.h"
#include "Pin.h"


class TouchButton {
public:
	Pin button;
	BitAction active_state;

	TouchButton(uint16_t _pin, BitAction _active_state = Bit_SET, FunctionalState _assign_interrupt = DISABLE);
	bool is_button_pressed();
};

#endif /* TOUCHBUTTON_TOUCHBUTTON_H_ */
