/*
 * Default pins:
 * 	nSTROBE -	PB12
 * 	D0		-	PB13
 * 	D1		-	PB14
 * 	D2		-	PB15
 * 	D3		-	PA8
 * 	D4		-	PA9
 * 	D5		-	PA10
 * 	D6		-	PA11
 * 	D7		-	PA12
 * 	nACK	-	PB6
 * 	BUSY	-	PB7
 * 	SLCT	-	PB8
 * 	nERROR	-	PB9
 */

#ifndef LPTPRINTER_H_
#define LPTPRINTER_H_

#include "stm32f10x.h"
#include "bluepill.h"
#include "Pin.h"

#define	STROBE_TIME_US	2

#define LPT_ESC			27
#define	CR				0x0A
#define LF				0x0D


class LPTPrinter {

private:
	Pin	strobe;
	Pin	data[8];
	Pin ack;
	Pin busy;
	Pin slct;
	Pin error;


public:
	LPTPrinter(void);
	void send_byte(uint8_t byte);
	void print_line(const char * text);
	void reset(void);
	bool is_printer_ready(void);

};


#endif /* LPTPRINTER_H_ */
