#include "LPTPrinter.h"

#include "stm32f10x.h"
#include "bluepill.h"
#include "Pin.h"
#include "micros.h"



LPTPrinter::LPTPrinter(void) {
	strobe  = Pin(PB12, GPIO_Mode_Out_PP, GPIO_Speed_2MHz, Bit_SET);
	data[0] = Pin(PB13, GPIO_Mode_Out_PP, GPIO_Speed_2MHz, Bit_RESET);
	data[1] = Pin(PB14, GPIO_Mode_Out_PP, GPIO_Speed_2MHz, Bit_RESET);
	data[2] = Pin(PB15, GPIO_Mode_Out_PP, GPIO_Speed_2MHz, Bit_RESET);
	data[3] = Pin(PA8, GPIO_Mode_Out_PP, GPIO_Speed_2MHz, Bit_RESET);
	data[4] = Pin(PA9, GPIO_Mode_Out_PP, GPIO_Speed_2MHz, Bit_RESET);
	data[5] = Pin(PA10, GPIO_Mode_Out_PP, GPIO_Speed_2MHz, Bit_RESET);
	data[6] = Pin(PA11, GPIO_Mode_Out_PP, GPIO_Speed_2MHz, Bit_RESET);
	data[7] = Pin(PA12, GPIO_Mode_Out_PP, GPIO_Speed_2MHz, Bit_RESET);
	ack 	= Pin(PB6, GPIO_Mode_IPU);
	busy 	= Pin(PB7, GPIO_Mode_IPD);
	slct 	= Pin(PB8, GPIO_Mode_IPD);
	error	= Pin(PB9, GPIO_Mode_IPU);

	micros_init();

	reset();

} //LPTPrinter()



void LPTPrinter::send_byte(uint8_t byte) {
	while (busy.digital_read() != 0);

	for (uint8_t i = 0; i < 8; ++i) {
		data[i].digital_write(byte & 0x1);
		byte = byte >> 1;
	}

	strobe.digital_write(Bit_RESET);
	delay_us(STROBE_TIME_US);
	strobe.digital_write(Bit_SET);

	while (busy.digital_read() != 0);

} //send_byte()



void LPTPrinter::print_line(const char * text) {
	uint8_t i = 0;

	while (text[i] != 0) {
		while (is_printer_ready() != true);
		send_byte(text[i]);
		i++;
	}
	//send_byte(CR);
} //print_line()



void LPTPrinter::reset(void) {
	while (is_printer_ready() != true);

	send_byte(LPT_ESC);
	send_byte('E');

	send_byte(17);		//24 - clears the buffer

	while (is_printer_ready() != true);
} //reset()



bool LPTPrinter::is_printer_ready(void) {
	return (bool) !busy.digital_read();
} //is_printer_ready()
