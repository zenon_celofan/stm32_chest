#include "WS2813Strip.h"
#include "stm32f10x.h"
#include "stm32f10x_spi.h"
#include "Spi.h"


WS2813Strip::WS2813Strip(u8 _length, SPI_TypeDef* _spi, bool _remap)
: length(_length) {

	SPI_InitTypeDef  SPI_InitStructure;
	SPI_InitStructure.SPI_Direction = SPI_Direction_1Line_Tx; //SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low; //SPI_CPOL_Low;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge; //SPI_CPHA_2Edge
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;      //_16
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB; //SPI_FirstBit_MSB
	SPI_InitStructure.SPI_CRCPolynomial = 7;
//	SPI_Init(_spi, &SPI_InitStructure);
//	SPI_Cmd(_spi, ENABLE);

	spi = new Spi(_spi, _remap, &SPI_InitStructure);

} //WS2813Strip()


WS2813Strip::~WS2813Strip(void) {
	delete spi;
} //~WS2813Strip()


void WS2813Strip::send_data_for_one_led(u32 _color) {
    // have to expand each bit to 3 bits
    // Can then output 110 for WS2812B logic '1'
    // and 100 for WS2812B logic '0'
    uint32_t Encoding=0;
    uint8_t SPI_Data[9];
    uint8_t Index;

    // Process the GREEN byte
    Index=0;
    Encoding=0;
    while (Index < 8)
    {
        Encoding = Encoding << 3;
        if (_color & (1 << 23))
        {
            Encoding |= 0b110;
        }
        else
        {
            Encoding |= 0b100;
        }
        _color = _color << 1;
        Index++;

    }
    SPI_Data[0] = ((Encoding >> 16) & 0xff);
    SPI_Data[1] = ((Encoding >> 8) & 0xff);
    SPI_Data[2] = (Encoding & 0xff);

    // Process the RED byte
    Index=0;
    Encoding=0;
    while (Index < 8)
    {
        Encoding = Encoding << 3;
        if (_color & (1 << 23))
        {
            Encoding |= 0b110;
        }
        else
        {
            Encoding |= 0b100;
        }
        _color = _color << 1;
        Index++;

    }
    SPI_Data[3] = ((Encoding >> 16) & 0xff);
    SPI_Data[4] = ((Encoding >> 8) & 0xff);
    SPI_Data[5] = (Encoding & 0xff);

    // Process the BLUE byte
    Index=0;
    Encoding=0;
    while (Index < 8)
    {
        Encoding = Encoding << 3;
        if (_color & (1 << 23))
        {
            Encoding |= 0b110;
        }
        else
        {
            Encoding |= 0b100;
        }
        _color = _color << 1;
        Index++;

    }
    SPI_Data[6] = ((Encoding >> 16) & 0xff);
    SPI_Data[7] = ((Encoding >> 8) & 0xff);
    SPI_Data[8] = (Encoding & 0xff);

    // Now send out the 24 (x3) bits to the SPI bus
    writeSPI(SPI_Data, 9);

} //send_data_for_one_led()


u32 WS2813Strip::rainbow_demo(void)
{   // Cycle through the colours of the rainbow (non-uniform brightness however)
	// Inspired by : http://academe.co.uk/2012/04/arduino-cycling-through-colours-of-the-rainbow/
	static unsigned Red = 255;
	static unsigned Green = 0;
	static unsigned Blue = 0;
	static uint16_t State = 0;
	switch (State)
	{
		case 0:{
			Green++;
			if (Green == 255)
				State = 1;
			break;
		}
		case 1:{
			Red--;
			if (Red == 0)
				State = 2;
			break;
		}
		case 2:{
			Blue++;
			if (Blue == 255)
				State = 3;
			break;
		}
		case 3:{
			Green--;
			if (Green == 0)
				State = 4;
			break;
		}
		case 4:{
			Red++;
			if (Red == 255)
				State = 5;
			break;
		}
		case 5:{
			Blue --;
			if (Blue == 0)
				State = 0;
			break;
		}
	}
	return (Green << 16) + (Red << 8) + Blue;
}


void WS2813Strip::led_strip_send(u32 * _colors) {
	for(u8 i = 0; i < length; ) {
		send_data_for_one_led(_colors[i++]);  //G R B
	}
} //led_strip_send()


void WS2813Strip::set_color(u32 _color) {
	u32 color_table[length];
	for(u8 i = 0; i < length; ++i) {
		color_table[i] = _color;
	}
	led_strip_send(color_table);
} //led_strip_send()

void WS2813Strip::writeSPI(u8 * _data, u8 _data_size) {
    while(_data_size--) {
    	spi->write_byte((u8)(*_data++));
        //SPI2->DR = (uint8_t)(*Data++);
        //while(SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET);
        //while(SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET);

        //while ((SPI2->SR & (1 << 1)) == 0); // wait if FIFO is full
    }
} //writeSPI


void WS2813Strip::clear(void) {
	for (u8 i = 0; i < length; ++i) {
		send_data_for_one_led(OFF);
	}
} //clear()
