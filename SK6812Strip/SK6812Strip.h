/*
 * Default SPI: SPI2, no remap
 * Pins:
 * 	SCK - PB13
 * 	MOSI - PB15
 * 	MISO - PB14
 * 	NSS - PB12
 */

#ifndef SK6812STRIP_H_
#define SK6812STRIP_H_

#include "stm32f10x.h"
#include "Spi.h"

// G - R - B
#define RED		0x00ff00
#define GREEN	0xff0000
#define BLUE	0x0000ff
#define YELLOW	0xffff00
#define PURPLE	0x00ffff
#define	AQUA	0xff00ff
#define WHITE	0xffffff
#define	OFF		0x000000


class SK6812Strip {

private:
	Spi*	spi;
	uint8_t length;

	void writeSK6812(u32 _data);
	void writeSPI(u8 * _data, u8 _data_size);
	void send_data_for_one_led(u32 _color);


public:
	SK6812Strip(uint8_t _length, SPI_TypeDef* _spi = SPI2, bool _remap = false);
	~SK6812Strip();
	void led_strip_send(u32 * _data);
	void set_color(u32 _color);
	void clear(void);
	u32 rainbow_demo(void);

};


#endif /* SK6812STRIP_H_ */
