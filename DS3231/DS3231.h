#ifndef DS3231_H_
#define DS3231_H_

//device address == 0x68. (0x68 << 1 == 0xD0)
//to read from ds3231 send slave address and then read up to 19 consecutive bytes, starting from pointer.
//to reset pointer write byte '0' to slave (send slave address and then send '0').
//to write to ds3231, send slave address, then address of register, then one or more bytes which will be written.

#include "stm32f10x.h"
#include "bluepill.h"
#include "stm32f10x_i2c.h"
#include "I2C.h"

#define DS3231_I2C_ADDRESS			0xD0
#define TIME_FORMAT_12_24H_FLAG		0b01000000
#define AM_PM_FLAG					0b00100000
#define TIME_FORMAT_12H				0b01000000
#define TIME_FORMAT_24H				0
#define PM							0b00100000
#define AM							0


struct Timestamp {
	uint8_t time_format = TIME_FORMAT_24H;
	uint8_t am_pm = AM;
	uint8_t seconds;
	uint8_t minutes;
	uint8_t hours;
	uint8_t short_time[4];
};



class DS3231 {

	I2C i2c;
	Timestamp timestamp;

public:
	DS3231(I2C_TypeDef* _i2c);
	Timestamp get_time(void);
	void set_time(Timestamp _time);
	void set_24h_time_format(void);
	void set_12h_time_format(void);
	void hours_increment(void);
	void hours_decrement(void);
	void minutes_increment(void);
	void minutes_decrement(void);
};

#endif /* DS3231_H_ */
