#include "DS3231.h"
#include "stm32f10x.h"
#include "bluepill.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_i2c.h"
#include "I2C.h"

DS3231::DS3231(I2C_TypeDef* _i2c) : i2c(_i2c) {
} //DS3231()


Timestamp DS3231::get_time(void) {
	Timestamp time;
	uint8_t buff[4];

	//send pointer address
	buff[0] = 0;
    i2c.master_write(buff, 1, DS3231_I2C_ADDRESS);

    //read data starting from pointer
	i2c.master_read(buff, 3, DS3231_I2C_ADDRESS);
	time.time_format = buff[2] & TIME_FORMAT_12_24H_FLAG;
//	time.short_time[1] =
	if (time.time_format == TIME_FORMAT_24H) {
		time.hours = (buff[2] & 0b00001111) + 10 * ((buff[2] >> 4) & 0b11);
	} else {
		time.hours = (buff[2] & 0b00001111) + 10 * ((buff[2] >> 4) & 0b01);
		time.am_pm = buff[2] & AM_PM_FLAG;
	}
	time.seconds = (buff[0] & 0b00001111) + 10 * (buff[0] >> 4);
	time.minutes = (buff[1] & 0b00001111) + 10 * (buff[1] >> 4);

	return time;
} //get_date_time()


void DS3231::set_time(Timestamp _time) {
	uint8_t buff[4];

	buff[3] = _time.time_format | _time.am_pm | (((_time.hours / 10) << 4) + (_time.hours % 10));
	buff[2] = ((_time.minutes / 10) << 4) + (_time.minutes % 10);
	buff[1] = ((_time.seconds / 10) << 4) + (_time.seconds % 10);
	buff[0] = 0;   //starting register address
	i2c.master_write(buff, 4, DS3231_I2C_ADDRESS);
} //set_time()


void DS3231::set_24h_time_format(void) {
	Timestamp time;

	time = get_time();

	if (time.time_format == TIME_FORMAT_24H) {
		return;
	} else {
		time.time_format = TIME_FORMAT_24H;
	}

	if (time.am_pm == PM) {
		if (time.hours < 12) {
			time.hours += 12;
		} else {
			time.hours -= 12;
		}
		time.am_pm = AM;
	}

	set_time(time);
} //set_24h_time_format()


void DS3231::set_12h_time_format(void) {
	Timestamp time;

	time = get_time();

	if (time.time_format == TIME_FORMAT_12H) {
		return;
	} else {
		time.time_format = TIME_FORMAT_12H;
	}

	if (time.hours >= 12) {
		time.am_pm = PM;
		if (time.hours > 12) {
			time.hours -= 12;
		}
	} else {
		time.am_pm = AM;
		if (time.hours == 0) {
			time.hours = 12;
		}
	}

	set_time(time);
} //set_24h_time_format()


void DS3231::hours_increment(void) {
	Timestamp time;

	time = get_time();

	if (time.time_format == TIME_FORMAT_24H) {
		if (time.hours == 23) {
			time.hours = 0;
		} else {
			time.hours++;
		}
	} else {
		time.hours++;
		if (time.hours == 13) {
			time.hours = 1;
		} else if (time.hours == 12) {
			if (time.am_pm == AM) {
				time.am_pm = PM;
			} else {
				time.am_pm = AM;
			}
		}
	}

	set_time(time);
} //hours_increment()


void DS3231::hours_decrement(void) {
	Timestamp time;

	time = get_time();

	if (time.time_format == TIME_FORMAT_24H) {
		if (time.hours == 0) {
			time.hours = 23;
		} else {
			time.hours--;
		}
	} else {
		time.hours--;
		if (time.hours == 0) {
			time.hours = 12;
		} else if (time.hours == 11) {
			if (time.am_pm == AM) {
				time.am_pm = PM;
			} else {
				time.am_pm = AM;
			}
		}
	}

	set_time(time);
} //hours_decrement()


void DS3231::minutes_decrement(void) {
	Timestamp time;

	time = get_time();

	if (time.minutes == 0) {
		time.minutes = 59;
	} else {
		time.minutes--;
	}
	time.seconds = 0;

	set_time(time);
} //minutes_decrement()


void DS3231::minutes_increment(void) {
	Timestamp time;

	time = get_time();

	if (time.minutes == 59) {
		time.minutes = 0;
	} else {
		time.minutes++;
	}
	time.seconds = 0;

	set_time(time);
} //minutes_increment()
