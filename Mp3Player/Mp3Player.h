#ifndef MP3PLAYER_H_
#define MP3PLAYER_H_

#include "stm32f10x_gpio.h"


#define BEEP_SOUND				1
#define WRONG_NUMBER_SOUND		2
#define NO_ANSWER_SOUND			3
#define BUSY_SOUND				4
#define CLICK_SOUND				5
#define FIRST_NUMBER_SOUND		6


#define	BUTTON_PRESS_TIME	100
#define BUTTON_RELEASE_TIME 50

class Mp3Player {

private:

	void send_status_request(void);

public:

	Mp3Player();

	bool is_stopped(void);
	void play(uint8_t);
	void play(uint32_t);
	void play_in_loop(uint8_t);
	void stop(void);
	void reset(void);

};


#endif /* MP3PLAYER_H_ */
